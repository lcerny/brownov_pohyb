#!/usr/bin/env python

import math
import random

def generuj(mu, sigma):
	u = random.uniform(0,1)
	v = random.uniform(0,1)

	z = math.sqrt(-2.0 * math.log(u)) * math.cos(2.0 * math.pi * v)
	xg = mu + z * sigma
	
	return xg

drahy = [100, 1000, 10]
for N in drahy:	
    os = 250
    
    pole=[[0 for x in range(500)] for y in range(N)] 
    for i in range(N):
        pole[i][0]=0
        for j in range(1, 500):
            pole[i][j] = pole[i][j-1] + generuj(0,1)


    k=0
    graf = open('graf'+str(N)+'.svg','w')
    graf.write('<?xml version=\"1.0\" standalone=\"no\"?>\n<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n <svg width=\"500\" height=\"500\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">\n')
    graf2 = open('graf_bodov'+str(N)+'.svg','w')
    graf2.write('<?xml version=\"1.0\" standalone=\"no\"?>\n<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n <svg width=\"500\" height=\"500\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">\n')
    for i in range(N):
        graf.write('<polyline points=" ')
        for j in range(500):
            graf.write(' ') 
            graf.write(str(j))
            graf.write(',')
            graf.write(str(os + pole[i][j]))
            graf.write(' ')
        if pole[i][10] > 0 and pole[i][20] > 0:
            graf.write('" style="fill:none;stroke:blue;stroke-width:0.5" />\n\n')
            k = k + 1
            graf2.write('<circle cx="')
            graf2.write(str(os + pole[i][10]))
            graf2.write('" cy="')
            graf2.write(str(os + pole[i][20]))
            graf2.write('" r="1" stroke="black" stroke-width="0.5" fill="blue" />\n') 
        else:
            graf.write('" style="fill:none;stroke:black;stroke-width:0.5" />\n\n')
            graf2.write('<circle cx="')
            graf2.write(str(os + pole[i][10]))
            graf2.write('" cy="')
            graf2.write(str(os + pole[i][20]))
            graf2.write('" r="1" stroke="black" stroke-width="0.5" fill="red" />\n')

    graf.write('</svg>')
    graf.close()
    graf2.write('</svg>')
    graf2.close()
    p = float(k) / float(N)
    print ('pravdepodobnost je:')
    print(str(p))
    print('pre pocet trajktorii:')
    print(str(N))
    print('\n')
